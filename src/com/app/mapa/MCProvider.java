package com.app.mapa;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
//import android.widget.Toast;

public class MCProvider extends ContentProvider {

	private static final String AUTH = "com.app.mapa.MCProvider";
	public static final Uri CP_URI = Uri.parse("content://" + AUTH + "/" + Bdoh.T_NOMBRE);
	public static String latitud; //variables que se usan en
	public static String longitud; //la actividad MapaLugares
	public static final String _ID = "_id";
	public static final int LUGARES = 1; //variables que se usan
	public static final int LUGARES_ID = 2; //en el m�todo getType
	private static final String TABLA_BD = "lugares";
	
	//especifica las diferentes formas que puede tener la uri del proveedor de contenidos
	private static final UriMatcher Umatcher;
	
	static
	{
		Umatcher = new UriMatcher(UriMatcher.NO_MATCH);
		Umatcher.addURI(AUTH, Bdoh.T_NOMBRE, LUGARES);
		Umatcher.addURI(AUTH, Bdoh.T_NOMBRE + "LUGARES/#", LUGARES_ID);
	}
	
	private SQLiteDatabase bd;
	//Bdoh bdHelper;
	
	@Override
	public boolean onCreate() 
	{
		
		//bdHelper = new Bdoh(getContext());
		
		Context context = getContext();
		Bdoh bdh = new Bdoh(context);
		bd = bdh.getWritableDatabase();
		
		return (bd == null) ? false:true;
	}
	
	@Override
	public Uri insert(Uri uri, ContentValues cvalores) 
	{
		
		//bd = bdHelper.getWritableDatabase();
		
		long IdFila = bd.insert(Bdoh.T_NOMBRE, "", cvalores);
		//Toast.makeText(MCProvider.this, "" + IdFila, Toast.LENGTH_LONG).show();
		if (IdFila >= 0) 
			return ContentUris.withAppendedId(CP_URI, IdFila);
		else 
			throw new SQLException("Error al insertar registro en " + uri);
		
	}
	
	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionargs, String sortorder) 
	{
		Cursor cr;
		
		cr = bd.query(Bdoh.T_NOMBRE, projection, selection, selectionargs, null, null, null);
		//para notificar que la uri se ha modificado al insertar registros
		cr.setNotificationUri(getContext().getContentResolver(), uri);
		
		return cr;
	}
	
	@Override
	public int delete(Uri u, String seleccion, String[] argseleccion) 
	{
		
		int flact = 0; //es el n�mero de filas actualizadas
				
		int urimt = Umatcher.match(u);
		
		//Toast.makeText(this, idlg, Toast.LENGTH_LONG).show();
		
		switch(urimt)
		{
			case LUGARES:
				flact = bd.delete(TABLA_BD, seleccion, argseleccion);
				break;
						
			case LUGARES_ID:
				String idlg = u.getLastPathSegment();
				flact = bd.delete(TABLA_BD, "_id = " + idlg + " and " + seleccion, argseleccion);
				break;
						
			default:
				throw new IllegalArgumentException("La uri " + u + "es desconocida");
		}
		
		return flact;
	}

	//es una cosa que usa Android y que se tiene que poner
	@Override
	public String getType(Uri uri) 
	{
		switch (Umatcher.match(uri)) 
		{
	        // para un conjunto de lugares
	        case LUGARES:
	            return "vnd.android.cursor.dir/vnd.llocs";
	
	        // para un solo lugar
	        case LUGARES_ID:
	            return "vnd.android.cursor.item/vnd.llocs";
	
	        default:
	        	throw new IllegalArgumentException("URI no soportada: " + uri);
        }
		
	}

	@Override
	public int update(Uri u, ContentValues cvalores, String seleccion, String[] selecargs) 
	{
		int flact = 0; //es el n�mero de filas actualizadas
		//String idlg = u.getPathSegments().get(1); //es para obtener el �ltimo par�metro de la uri
		
		int urimt = Umatcher.match(u);
		
		//Toast.makeText(this, idlg, Toast.LENGTH_LONG).show();
		
		switch(urimt)
		{
			case LUGARES:
				flact = bd.update(TABLA_BD, cvalores, seleccion, selecargs);
				break;
						
			case LUGARES_ID:
				String idlg = u.getLastPathSegment();
				flact = bd.update(TABLA_BD, cvalores, "_id = " + idlg + " and " + seleccion, selecargs);
				break;
						
			default:
				throw new IllegalArgumentException("La uri " + u + "es desconocida");
		}
		
		return flact;
	}
}
