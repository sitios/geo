package com.app.mapa;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Lugar {

	private long iden; //es el id de la tabla de los sitios
	private String nomlugar;
	private String descriplugar;
	private double lnglugar;
	private double latlugar;
	private String fotolugar;
	//private int imagen;
	
	public Lugar(String nlugar, String dlugar, double lglugar, double ltlugar, String flugar)
	{
		//this.iden = idt;
		this.nomlugar = nlugar;
		this.descriplugar = dlugar;
		this.lnglugar = lglugar;
		this.latlugar = ltlugar;
		this.fotolugar = flugar;
	}
	
	public long getIdentificador()
	{
		return iden;
	}
	
	public void setIdentificador(long idn)
	{
		this.iden = idn;
	}
	
	public String getNombre()
	{
		return nomlugar;
	}

	public void setNombre(String nmlugar) 
	{
		this.nomlugar = nmlugar;
	}

	public String getDescripcion()
	{
		return descriplugar;
	}
	
	public void setDescripcion(String dsclugar) 
	{
		this.descriplugar = dsclugar;
	}
	
	public double getlongitud()
	{
		return lnglugar;
	}

	public void setLongitud(double loglugar) 
	{
		this.lnglugar= loglugar;
	}
	
	public double getlatitud()
	{
		return latlugar;
	}
	
	public void setLatitud(double ltlugar) 
	{
		this.latlugar = ltlugar;
	}
	
	public String getfotolugar()
	{
		return fotolugar;
	}

	public void setFoto(String ftlugar) 
	{
		this.fotolugar = ftlugar;
	}

	/*public int getImagen()
	{
		return imagen;
	}
	
	public void setImagen(int pimg)
	{
		this.imagen = pimg;
	}*/

	//Carga la imagen en la memoria desde la ruta
	public Bitmap obtenerImagendelaRuta(String filePath,int reqWidth, int reqHeight) 
	{
		    
		BitmapFactory.Options options = new  BitmapFactory.Options();
		options.inJustDecodeBounds = true; 
		BitmapFactory.decodeFile(filePath, options);
		options.inSampleSize = calcularTamanoImagen(options, reqWidth, reqHeight);
		options.inJustDecodeBounds = false;
		
		return BitmapFactory.decodeFile(filePath, options);
	}
		
	//calcula el factor de escala
	private int calcularTamanoImagen(BitmapFactory.Options options, int reqWidth, int reqHeight) 
	{
		    
		// Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int tamanoImagen = 1;

	    if (height > reqHeight || width > reqWidth) 
	     {

	        // Calculate ratios of height and width to requested height and width
	        final int heightRatio = Math.round((float) height / (float) reqHeight);
	        final int widthRatio = Math.round((float) width / (float) reqWidth);

	        // Choose the smallest ratio as inSampleSize value, this will guarantee
	        // a final image with both dimensions larger than or equal to the
	        // requested height and width.
	        tamanoImagen = heightRatio < widthRatio ? heightRatio : widthRatio;
	     }

		    return tamanoImagen;
	}
}
