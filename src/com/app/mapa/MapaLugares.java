package com.app.mapa;

import android.net.Uri;
import android.os.Bundle;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Point;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
//import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapaLugares extends FragmentActivity implements OnMarkerClickListener {

	private GoogleMap mMap;
	private Marker marcador;
	SQLiteDatabase db;
	Cursor crs;
		
	//@SuppressWarnings("resource")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mapa_lugares);
		final Uri C_URI = MCProvider.CP_URI;
		final String[] clm = {"_id","nombre","latitud","longitud"};
				
		mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		
		crs = getContentResolver().query(C_URI, clm, null, null, null);
		if(crs!=null) 
		 {	
			crs.moveToFirst(); 
			while(crs.isAfterLast() == false)
			{	
				try 
				  {
					if(crs!=null)
					 {	
						//posiciona los marcadores en el mapa
						final LatLng  posicion = new LatLng(crs.getDouble(crs.getColumnIndex("latitud")), crs.getDouble(crs.getColumnIndex("longitud")));
						/*Toast.makeText(MapaLugares.this, "Latitud" + crs.getString(crs.getColumnIndex("latitud")), Toast.LENGTH_SHORT)
						.show();	
						Intent inten = new Intent();
						inten.setAction(android.content.Intent.ACTION_VIEW);
						inten.setData(Uri.parse("geo:"+crs.getString(crs.getColumnIndex("latitud"))+","+crs.getString(crs.getColumnIndex("longitud"))));
						startActivity(inten);*/
						String nl = crs.getString(crs.getColumnIndex("nombre"));
						//marcador = mMap.addMarker(new MarkerOptions().position(posicion).title(nl));
						marcador = mMap.addMarker(new MarkerOptions().position(posicion));
					 }	
				  }
				 
				 finally
				       {
					 	 crs.moveToNext();
				       }
			}		
		 }		
		
		//para poner un listener en los marcadores del mapa para cuando se haga clic se edite el
		//lugar
		mMap.setOnMarkerClickListener(this);
		
		mMap.setOnMapClickListener(new OnMapClickListener() 
		{

			@Override
			public void onMapClick(LatLng point) 
			{
				Projection proj = mMap.getProjection();
				Point coord = proj.toScreenLocation(point);
				/*final String s = com.app.mapa.MCProvider.latitud + " = " + point.latitude + 
						"AND" + com.app.mapa.MCProvider.longitud + " = " + point.longitude;*/
				
				//pasamos la latitud y la longitud a la actividad InsertarLugar para guardarlos en
				//la base de datos
				Intent intencion = new Intent(MapaLugares.this,Insertarlugar.class);
				intencion.putExtra("latitud", point.latitude);
				intencion.putExtra("longitud", point.longitude);
				startActivity(intencion);				
				
				//Toast.makeText(MapaLugares.this, "Click\n" + "Lat: " + point.latitude + "\n" + "Lng: " + point.longitude + "\n" + "X: " + coord.x + " - Y: " + coord.y, Toast.LENGTH_SHORT)
				//.show();	
			 }

		});
		
	}	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mapa_lugares, menu);
		return true;
	}
	
	@Override
	protected void onDestroy() 
	{
		//el cursor se cierra cuando la actividad se destruye debido a que se necesita para mostrar
		//los lugares en el mapa y tambi�n podr�a usarse para editar el lugar si el usuario hace 
		//clic en �l
		crs.close();
		super.onDestroy();
	}
	
	//si se hace clic en un marcador se edita el lugar llamando a EditarLugares
	@Override
	public boolean onMarkerClick(Marker mc) 
	{
		//se obtiene el id del lugar de la base de datos
		//long idlg = crs.getLong((crs.getColumnIndex("_id")));
		
		long mcid = 0; //para guardar el id del lugar clicado por el usuario
		LatLng mctl = mc.getPosition(); //para guardar la posici�n del lugar clicado por el usuario
		/*Toast.makeText(MapaLugares.this, "" + mctl, Toast.LENGTH_SHORT)
		.show();*/ 
		//se busca la posici�n del lugar clicado para despu�s obtener su id
		crs.moveToFirst();
		while(mctl.latitude!=crs.getDouble(crs.getColumnIndex("latitud")) && 
			  mctl.longitude!=crs.getDouble(crs.getColumnIndex("longitud")))
			{
				crs.moveToNext();
			}
		
		//se obtiene el id del lugar clicado
		mcid = crs.getLong(crs.getColumnIndex("_id"));
		/*Toast.makeText(MapaLugares.this, "" + mcid, Toast.LENGTH_SHORT)
		.show();*/
		//se pasa el par�metro id del lugar a la actividad EditarLugares		
		Intent intencio = new Intent(MapaLugares.this,EditarLugares.class);
		intencio.putExtra("lugarid", mcid);
		startActivity(intencio);
		
		return false;
	}
}
