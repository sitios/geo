package com.app.mapa;

import java.util.ArrayList;

import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
//import android.widget.Toast;

public class Mostrarlugares extends Activity {

	Uri urititl = MCProvider.CP_URI;
	String[] cols = {"_id","nombre","descripcion","latitud","longitud","foto"};
	private ArrayList<Lugar> lugrs;
	Cursor cr = null;
			
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mostrarlugares);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mostrarlugares, menu);
		return true;
	}

	//saca de la base de datos todos los registros y los guarda en un cursor 
	//para insertar todos los lugares de la base de datos en la clase Lugar
	public Cursor insertlugares()
	{
		cr = getContentResolver().query(urititl, cols, null, null, null);
		
		return cr;
	}

	//inserta los registros del cursor cr al ArrayList para poder mostrar los datos en el ListView
	//permite editar o borrar cualquier lugar
	public void llenarlugares(Cursor cl)
	{
		ListView lview;
		//se llama a la actividad EditarLugares para editar un lugar
		final Intent inten = new Intent(this,EditarLugares.class); 
				
		//comprobamos que el resultado de la consulta no sea null
		if(cl != null) 
		 {
			//si hay al menos un registro en la bd lo guardamos en el ArrayList		
			if(cl.getCount() != 0) 
			{
				//nos situamos en el primer registro
				cl.moveToFirst();
				while(cl.isAfterLast() == false)
				{	
					//Toast.makeText(Mostrarlugares.this, "" + cl.getString(cl.getColumnIndex("nombre")), Toast.LENGTH_LONG).show();
					Lugar l = new Lugar("","",0.0,0.0,"");
					
					l.setIdentificador(cl.getLong(cl.getColumnIndex("_id")));
					//l.setIdentificador((cl.getColumnIndex("_id")));
					l.setNombre(cl.getString(cl.getColumnIndex("nombre")));
					l.setDescripcion(cl.getString(cl.getColumnIndex("descripcion")));
					l.setLongitud(cl.getDouble((cl.getColumnIndex("longitud"))));
					l.setLatitud(cl.getDouble(cl.getColumnIndex("latitud")));
					l.setFoto(cl.getString(cl.getColumnIndex("foto")));
					//Toast.makeText(Mostrarlugares.this, "" + l, Toast.LENGTH_LONG).show();
					lugrs.add(l);
					//Toast.makeText(Mostrarlugares.this, "" + l.getIdentificador(), Toast.LENGTH_LONG).show();
					cl.moveToNext();
				}	
			 }
		 }
		
		//musetra la lista de lugares a trav�s del adaptador
		lview = (ListView) findViewById(R.id.lista);
		LugarAdapter lugadaptador = new LugarAdapter(this,lugrs);
		lview.setAdapter(lugadaptador);
		
		lview.setOnItemClickListener(new OnItemClickListener() {
			
			//para editar o borrar el lugar
			@Override
			public void onItemClick(AdapterView<?> av, View v, int pos, long id) 
			{
				final long lid;
				//para determinar que lugar se ha pulsado
				Lugar lug = lugrs.get(pos);
				//Toast.makeText(Mostrarlugares.this, lug.getNombre(), Toast.LENGTH_LONG).show();
				lid = lug.getIdentificador();
								
				//se crea el cuadro de di�logo para seleccionar la edici�n o el borrado del lugar
				final String[] items = {"Editar", "Borrar"};
								
		         AlertDialog.Builder builder = new AlertDialog.Builder(Mostrarlugares.this);
		         builder.setTitle("Lugar");
		         builder.setItems(items, new DialogInterface.OnClickListener() {
		             public void onClick(DialogInterface dialog, int item) {
		                 
		            	 //se llama a la actividad BorrarLugares para borrar un lugar
		 				 Intent intenborrar = new Intent(Mostrarlugares.this,BorrarLugares.class);
		            	 
		            	 //En funci�n de lo que se escoja se va a editar o a borrar lugar
		            	 switch (item)
		            	 {
		            	 	//se escoge la opci�n de edici�n del lugar
		            	 	case 0:
		            	 	{
		            	 		//Se le pasa el par�metro id del lugar a la actividad EditarLugares
		            	 		inten.putExtra("lugarid", lid);
		            	 		startActivity(inten);
		            	 		break;
		            	 	}
		            	 	//se escoge la opci�n de borrar el lugar
		            	 	case 1:
		            	 	{
		            	 		//se le pasa el par�metro id del lugar para borrar el lugar especificado
		            	 		intenborrar.putExtra("lugarid", lid);
		            	 		startActivity(intenborrar);
		            	 		//Toast.makeText(Mostrarlugares.this, "Borrado del lugar", Toast.LENGTH_LONG).show();
		            	 		break;
		            	 	}
		            	 }
		             }
		         });
			
		         AlertDialog alert = builder.create();
		         alert.show();
			}
			
		});
		//lview.setOnClickListener(this);
		//para guardar en un String el contenido del cursor
		//String cursorDump = DatabaseUtils.dumpCursorToString(cl);
		//Toast.makeText(Mostrarlugares.this, "" + cursorDump, Toast.LENGTH_LONG).show();
		//Toast.makeText(Mostrarlugares.this, "" + cl.getColumnIndex("latitud"), Toast.LENGTH_LONG).show();
		cl.close();
		cr.close();
	 }
	
	@Override
	protected void onStart() {
		
		lugrs = new ArrayList<Lugar>();
		Cursor crr = null;
		//guarda todos los registros de la bd en el cursor crr
		crr = insertlugares();
		//guarda los datos del cursor a la clase Lugar
		llenarlugares(crr);
		crr.close();
		
		super.onStart();
	}
}
