package com.app.mapa;

import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
//import android.widget.Toast;

public class EditarLugares extends Activity {

	long idlug = 0; //es el id del lugar que se ha seleccionado
	Cursor cru = null; //es el cursor que se usa para recuperar los datos del lugar seleccionado
	EditText titlugr; //para poner el t�tulo del lugar
	EditText desclugr; //para poner la descripci�n del lugar
	ImageView fotolugr; //para poner la imagen del lugar
	int imgAncho = 512; //ancho que se quiere de la imagen
	int imgAltura = 512; //altura que se quiere de la imagen
	Button mboton; //para el bot�n de modificar
	final Uri urititul = MCProvider.CP_URI;
	ContentValues cvcolms = new ContentValues();
	private static int CAPTURA_FOTO = 2; //si se accede a la c�mara
	private static int SELECT_FOTO = 1; //si se accede a a la galeria
	int cdg = 0;
	String sfoto; //guarda la ruta de la foto
	private boolean pulsa = false; //para saber si se ha modificado la foto
	private String flugar;
	
	Lugar lur = new Lugar("","",0.0,0.0,""); //se crea un objeto Lugar para poder usar el m�todo 
											 //obtenerImagendelaRuta	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_editar_lugares);
		
		final String[] clums = {"_id","nombre","descripcion","latitud","longitud","foto"};
				
		//se recibe el par�metro id del lugar desde la actividad Mostrarlugares y MapaLugares
		//Bundle b = getIntent().getExtras();
		idlug = getIntent().getExtras().getLong("lugarid");
		String wh = "_id = '" + idlug + "'"; //es el where de la consulta
		
		//Otra manera de pasar los par�metros
		/*idlug = getIntent().getLongExtra("lugarid", -1);
		if(idlug != -1) Toast.makeText(EditarLugares.this, "Lugar con id " + idlug, Toast.LENGTH_LONG).show();
		else Toast.makeText(EditarLugares.this, "Lugar no encontrado", Toast.LENGTH_LONG).show();*/
		
		//obtiene de la base de datos los datos del id del lugar seleccionado
		cru = getContentResolver().query(urititul, clums, wh, null, null);
		
		//situa el puntero al principio del cursor
		cru.moveToFirst();
				
		//muestra los datos en pantalla: t�tulo, descripci�n y foto
		titlugr = (EditText) findViewById(R.id.edit_titulo_lugar);
		titlugr.setText(cru.getString(cru.getColumnIndex("nombre")));
		//Toast.makeText(EditarLugares.this, "" + tl, Toast.LENGTH_LONG).show();
				
		desclugr = (EditText) findViewById(R.id.edit_descripcion_lugar);
		desclugr.setText(cru.getString(cru.getColumnIndex("descripcion")));
		
		fotolugr = (ImageView) findViewById(R.id.imgFoto);
		final String rutaimg = cru.getString(cru.getColumnIndex("foto")); 
		Bitmap bitmap = lur.obtenerImagendelaRuta(rutaimg, imgAncho, imgAltura);
	    fotolugr.setImageBitmap(bitmap);
	    
	    //listener para poder modificar la foto
	    fotolistener();
	    
	    //listener para el bot�n modificar
	    mboton = (Button) findViewById(R.id.modificar);
		
		mboton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
				String nomblugar = titlugr.getText().toString();
				String desclugar = desclugr.getText().toString();
				if (pulsa == false) flugar = rutaimg;
				else flugar = sfoto;
								
				cvcolms.put("nombre", nomblugar);
				cvcolms.put("descripcion", desclugar);
				cvcolms.put("foto", flugar);
				//Toast.makeText(EditarLugares.this, "Valor de rutaimg " + rutaimg, Toast.LENGTH_LONG).show();
				getContentResolver().update(urititul, cvcolms, "_id = " + idlug, null);
				finish(); //para finalizar la actividad una vez se ha hecho la modificaci�n
			}	
		});		
	}
	
	public void fotolistener()
	{
		fotolugr = (ImageView) findViewById(R.id.imgFoto);
		
		fotolugr.setOnClickListener(new OnClickListener() { //cuando se aprieta en el imagebutton

			@Override
			public void onClick(View arg0) {
				
				//se crea el cuadro de di�logo para seleccionar la camara o la galeria
				final String[] items = {"C�mara", "Galer�a"};
								
		         AlertDialog.Builder builder = new AlertDialog.Builder(EditarLugares.this);
		         builder.setTitle("Foto");
		         builder.setItems(items, new DialogInterface.OnClickListener() {
		             public void onClick(DialogInterface dialog, int item) {
		                 
		            	//Si se escoge c�mara se va a la c�mara de hacer fotos
		            	 switch (item)
		            	 {
		            	 	//se accede a la galer�a
		            	 	case 1:
		            	 	{
		            	 		Intent fotoitn = new Intent(Intent.ACTION_PICK);
		            	 		fotoitn.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
		            	 		MediaStore.Images.Media.CONTENT_TYPE);
		            	 		cdg = SELECT_FOTO;
		            	 		startActivityForResult(Intent.createChooser(fotoitn,"Selecciona una imagen"),cdg);
		            	 		break;
		            	 	}
		            	 	//se accede a la c�mara
		            	 	case 2:
		            	 	{
		            	 		Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
		                        cdg = CAPTURA_FOTO;
		            	 		startActivityForResult(cameraIntent, cdg);
		            	 		break;
		            	 	}
		            	 }
		             }
		         });
		         
		         AlertDialog alert = builder.create();
		         alert.show(); 
		     }
		});
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		int ancho = 512;
		int altura = 512;
		
		if(resultCode == RESULT_OK) 
		{	
			//si la foto proviene de la galeria
			if(requestCode == SELECT_FOTO) 
			{
				Uri ImagenSelec = data.getData();
		        sfoto = rutaUri(ImagenSelec);
				//Toast.makeText(Insertarlugar.this, "Ruta de la imagen " + sfoto, Toast.LENGTH_LONG).show();
		        //String[] filePathColumn = {MediaStore.Images.Media.DATA};
		        cvcolms.put("foto", sfoto);
		        pulsa = true; //se ha modificado la foto del lugar
		        //Toast.makeText(EditarLugares.this, "Valor de sfoto " + sfoto, Toast.LENGTH_LONG).show();
		        //para insertar la imagen en el ImageView
		        Bitmap bitmap = lur.obtenerImagendelaRuta(sfoto, ancho, altura);
		        fotolugr.setImageBitmap(bitmap);
			}
			//si la foto se hace con la c�mara
			if(requestCode == CAPTURA_FOTO) 
			{
				//Camera cm;
				//Toast.makeText(EditarLugares.this, "Se obtiene la foto de la c�mara", Toast.LENGTH_LONG).show();
			}
		}	
	}

	//obtiene la ruta de la imagen seleccionada
	public String rutaUri(Uri conUri)
	{
		Cursor csr = getContentResolver().query(conUri, null, null, null, null);
		if (csr == null) 
		{
			return conUri.getPath();
		} 
		
		else 
			 {
				csr.moveToFirst();
				int idx = csr.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
				return csr.getString(idx);
			 }
	}

	//para que el cursor se cierre despu�s de que el usuario haya pulsado el bot�n modificar
	//no se puede poner en el onCreate porqu� se ejecuta todo a la vez excepto el onClick que lo 
	//hace cuando se ha pulsado el bot�n modificar
	@Override
	protected void onDestroy() 
	{
		cru.close();
		super.onDestroy();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.editar_lugares, menu);
		return true;
	}
}
