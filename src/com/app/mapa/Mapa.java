package com.app.mapa;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;

public class Mapa extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mapa);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menuopc, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem mt)
    {
    	super.onOptionsItemSelected(mt);
    	
    	switch (mt.getItemId())
    	{
    		case R.id.mapa:
    			opmapa();
    			break;	
    	
    		case R.id.lista:
    			oplista();
    			break;
      	}
    	return true;
    }
    	
    private void opmapa()
    {
    	startActivity(new Intent (Mapa.this,MapaLugares.class));
    }
    
    private void oplista()
    {
    	startActivity(new Intent (Mapa.this,Mostrarlugares.class));
    }
}
