package com.app.mapa;

import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.text.TextUtils;
import android.view.Menu;
import android.widget.*;
import android.view.View.OnClickListener;
import android.view.View;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;


public class Insertarlugar extends Activity {

	protected static final int RESULT_LOAD_IMAGE = 1;
	ImageView imgfto; //donde se inserta la foto del lugar
	Button gboton; //para el bot�n guardar
	private static int CAPTURA_FOTO = 2; //si se accede a la c�mara
	private static int SELECT_FOTO = 1; //si se accede a a la galeria
	
	//variables para guardar los valores en la base de datos
	EditText titlugar; 
	EditText desclugar;
	String tlugar;
	String dlugar;
	double lat;
	double lgt;
	String sfoto;
	int cdg = 0;
	
	//creaci�n de un objeto lugar para poder acceder a la funci�n de carga de la imagen en el 
	//imageView
	Lugar lgr = new Lugar("","",0.0,0.0,"");
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_insertarlugar);
				
		imgfto = (ImageView) findViewById(R.id.imgFoto);
		
		imgfto.setOnClickListener(new OnClickListener() { //cuando se aprieta en el imagebutton

			@Override
			public void onClick(View arg0) {
				
				//se crea el cuadro de di�logo para seleccionar la camara o la galeria
				final String[] items = {"C�mara", "Galer�a"};
								
		         AlertDialog.Builder builder = new AlertDialog.Builder(Insertarlugar.this);
		         builder.setTitle("Foto");
		         builder.setItems(items, new DialogInterface.OnClickListener() {
		             public void onClick(DialogInterface dialog, int item) {
		                 
		            	//Si se escoge c�mara se va a la c�mara de hacer fotos
		            	 switch (item)
		            	 {
		            	 	//se accede a la galer�a
		            	 	case 1:
		            	 	{
		            	 		Intent fotoitn = new Intent(Intent.ACTION_PICK);
		            	 		fotoitn.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
		            	 		MediaStore.Images.Media.CONTENT_TYPE);
		            	 		cdg = SELECT_FOTO;
		            	 		startActivityForResult(Intent.createChooser(fotoitn,"Selecciona una imagen"),cdg);
		            	 		break;
		            	 	}
		            	 	//se accede a la c�mara
		            	 	case 2:
		            	 	{
		            	 		Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
		                        cdg = CAPTURA_FOTO;
		            	 		startActivityForResult(cameraIntent, cdg);
		            	 		break;
		            	 	}
		            	 }
		             }
		         });
		         
		         AlertDialog alert = builder.create();
		         alert.show(); 
		     }
		});
		
		BotonListener();
	}

	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		int colIndice = 0;
		int ancho = 512;
		int altura = 512;
		
		if(resultCode == RESULT_OK) 
		{	
			//si la foto proviene de la galeria
			if(requestCode == SELECT_FOTO) 
			{
				Uri ImagenSelec = data.getData();
		        sfoto = rutaUri(ImagenSelec);
				//Toast.makeText(Insertarlugar.this, "Ruta de la imagen " + sfoto, Toast.LENGTH_LONG).show();
		        //String[] filePathColumn = {MediaStore.Images.Media.DATA};
		        
		        //para insertar la imagen en el ImageView
		          Bitmap bitmap = lgr.obtenerImagendelaRuta(sfoto, ancho, altura);
		          imgfto.setImageBitmap(bitmap);
			}
			//si la foto se hace con la c�mara
			if(requestCode == CAPTURA_FOTO) 
			{
				//Camera cm;
			}
		}	
	}

	//obtiene la ruta de la imagen seleccionada
	public String rutaUri(Uri conUri)
	{
		Cursor csr = getContentResolver().query(conUri, null, null, null, null);
		if (csr == null) 
		{
			return conUri.getPath();
		} 
		
		else 
			 {
				csr.moveToFirst();
				int idx = csr.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
				return csr.getString(idx);
			 }
	}
	
	public void BotonListener()
	{
		gboton = (Button) findViewById(R.id.guardar);
		
		gboton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				//pasamos a String los datos introducidos por el usuario en los edittext
				titlugar = (EditText) findViewById(R.id.edit_titulo_lugar);
				tlugar = titlugar.getText().toString();
						
				desclugar = (EditText) findViewById(R.id.edit_descripcion_lugar);
				dlugar = desclugar.getText().toString();
				
				//Obtenemos la latitud y la longitud de la actividad MapaLugares para guardarla en 
				//la base de datos
				Bundle parametros = getIntent().getExtras();
				lat = parametros.getDouble("latitud");
				lgt = parametros.getDouble("longitud");
				
				//obtenemos la uri de las im�genes de los lugares para guardarlas en la base de datos
				//Uri ufoto = Uri.parse("android.resource://" + getPackageName() + "/" + R.drawable.camara1);
				//pasamos a string la uri obtenida
				//sfoto = ufoto.toString();
				
				//comprobamos que los datos que se van a introducir no est�n repetidos
				boolean igual = iguales();
				//Toast.makeText(Insertarlugar.this, "" + igual, Toast.LENGTH_LONG).show();
				if(!igual) 
				 {
					if((tlugar!=null) && (dlugar!=null) && (sfoto!=null))
					{	
						//guardamos los datos introducidos en la base de datos
						ContentValues valores = new ContentValues();
						valores.put("nombre", tlugar);
						valores.put("descripcion", dlugar);
						valores.put("latitud", lat);
						valores.put("longitud", lgt);
						valores.put("foto", sfoto);
						
						//insertamos los datos en la base de datos
						Uri nelemento = getContentResolver().insert(MCProvider.CP_URI, valores);
						//despu�s de insertar los datos en la bd se acaba la actividad
						finish();
						//Toast.makeText(Insertarlugar.this, "" + nelemento, Toast.LENGTH_LONG).show();
						//Toast.makeText(Insertarlugar.this, "" + sfoto, Toast.LENGTH_LONG).show();
						//Toast.makeText(Insertarlugar.this, "Se guardan los datos", Toast.LENGTH_LONG).show();
					}
					else 
					{
						Toast.makeText(Insertarlugar.this, "Lugar no insertado", Toast.LENGTH_LONG).show();
						//despu�s de insertar los datos en la bd se acaba la actividad
						finish();
					}
				 }	
			}
		});
	}
	
	public boolean iguales() //comprueba si el nombre del sitio ya est� introducido
	{
		boolean ig = false;
		Cursor c = null;
		
		//Uri urititl = Uri.parse("content://com.app.mapa.MCProvider /" + Bdoh.T_NOMBRE);
		Uri urititl = MCProvider.CP_URI;
		String[] cols = {"nombre"};
		//String sel = "nombre = " + titlugar + ")groupby nombre having lo que sea";
		String sel = "nombre = '" + tlugar + "'";
		//se hace la consulta para recuperar los registros
		if(!TextUtils.isEmpty(tlugar)) 
		c = getContentResolver().query(urititl,cols,sel,null,null);
		
		//se comprueba que el resultado de la consulta no sea null
		if(c != null) 
		{
			//nos situamos en el primer registro
			c.moveToFirst();
			
			if(c.getCount() != 0) ig = true;
		}
		
		//cerramos el cursor
		c.close();
		//Toast.makeText(Insertarlugar.this, "" + ig, Toast.LENGTH_LONG).show();		
		return ig;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.insertarlugar, menu);
		
		return true;
	}
}
