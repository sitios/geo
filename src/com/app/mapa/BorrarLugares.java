package com.app.mapa;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Menu;

public class BorrarLugares extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_borrar_lugares);
		
		long idlg = 0; //para guardar el id del lugar seleccionado en EditarLugares
		//se guarda en idlg el par�metro idlug que proviene de EditarLugares
		idlg = getIntent().getExtras().getLong("lugarid");
		final Uri urititul = MCProvider.CP_URI; //especificaci�n de la uri para el borrado 
		final String wh = "_id = '" + idlg + "'"; //es el where de la consulta
				
		//para mostrar la ventana para asegurarse si se quiere borrar el lugar o no
		DialogInterface.OnClickListener dialoglistener = new DialogInterface.OnClickListener() 
		{
			
			@Override
			public void onClick(DialogInterface dialog, int which) 
			{
				
				switch(which)
				{
					//si se hace clic en la opci�n afirmativa se borra el lugar 
					case DialogInterface.BUTTON_POSITIVE:
					{
						getContentResolver().delete(urititul, wh, null);
						finish();
						break;
					}
					//si se hace clic en la opci�n negativa se acaba esta actividad	
					case DialogInterface.BUTTON_NEGATIVE:
					{
						finish();
						break;
					}
				
				}
			}
		};
	
		AlertDialog.Builder bli = new AlertDialog.Builder(this);
		bli.setMessage("�Est�s seguro?").setPositiveButton("Si", dialoglistener).
		setNegativeButton("No", dialoglistener).show();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.borrar_lugares, menu);
		return true;
	}
}
