package com.app.mapa;

import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
//import android.widget.Toast;

public class LugarAdapter extends BaseAdapter
{
	Context contexto;
	private List<Lugar> dlugares;
	Lugar lgu = new Lugar("","",0.0,0.0,"");
	
	public LugarAdapter(Context contex, List<Lugar> litems) 
	{
        this.contexto = contex;
        this.dlugares = litems;
    }
	
	private class ViewHolder 
	{
        ImageView fotoView; //para la foto del lugar
        TextView txtTitulo; //para el titulo del lugar
        TextView txtDesc; //para la descriptci�n del lugar
    }
	
	//el holder es para guardar vistas hijo
	//el m�todo getView es para situar los items del ListView
	public View getView(int posicion, View convertView, ViewGroup parent) 
	{
        ViewHolder holder = null;
        int imgAncho = 75;
        int imgAltura = 75;
        String rutaimg = null;
         
        LayoutInflater mInflater = (LayoutInflater)
            contexto.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) 
        {
            convertView = mInflater.inflate(R.layout.lista_item, null);
            holder = new ViewHolder();
            holder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
            holder.txtTitulo = (TextView) convertView.findViewById(R.id.titl);
            holder.fotoView = (ImageView) convertView.findViewById(R.id.flugar);
            convertView.setTag(holder);
        }
        else 
        {
            holder = (ViewHolder) convertView.getTag();
        }
 
        Lugar lugItem = (Lugar) getItem(posicion);
 
        holder.txtDesc.setText(lugItem.getDescripcion());
        holder.txtTitulo.setText(lugItem.getNombre());
        rutaimg = lugItem.getfotolugar();
        Bitmap bitmap = lugItem.obtenerImagendelaRuta(rutaimg, imgAncho, imgAltura);
        holder.fotoView.setImageBitmap(bitmap);
        //Log.i("Rutaimg", rutaimg);
        //Toast.makeText(this, "" , Toast.LENGTH_LONG).show();
                
        return convertView;
    }

	@Override
    public int getCount() {
        return dlugares.size();
    }
 
    @Override
    public Object getItem(int position) {
        return dlugares.get(position);
    }
 
    @Override
    public long getItemId(int poscion) {
        return dlugares.indexOf(getItem(poscion));
    }
}	