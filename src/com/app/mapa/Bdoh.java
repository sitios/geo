package com.app.mapa;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class Bdoh extends SQLiteOpenHelper
{
	//private static final String bdcons = "CREATE TABLE lugares (_id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT, descripcion TEXT, latitud DOUBLE, longitud DOUBLE, foto TEXT)";
	
	SQLiteOpenHelper sbd;
	static String T_NOMBRE="lugares"; //para el content provider
	private static final String NOMBRE_BD = "Sitios";
	private static final int VER_BD = 1;
	private static final String bdcons = "CREATE TABLE " + T_NOMBRE + 
	"(_id INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT, descripcion TEXT, latitud DOUBLE, " +
	"longitud DOUBLE, foto TEXT)";
		
	public Bdoh(Context context) 
	{
		super(context, NOMBRE_BD, null, VER_BD);
		SQLiteDatabase db=context.openOrCreateDatabase(NOMBRE_BD,Context.MODE_PRIVATE,null); //crea la base de datos
		//context.deleteDatabase(NOMBRE_BD); //para borrar la base de datos creada
	}

	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		// TODO Auto-generated method stub
		db.execSQL(bdcons);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		// TODO Auto-generated method stub
		//Se elimina la versi�n anterior de la tabla
        db.execSQL("DROP TABLE IF EXISTS lugares");
 
        //Se crea la nueva versi�n de la tabla
        db.execSQL(bdcons);
	}
}
